function myMap() {
  var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
    scrollwheel:  false
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
  // Enable scrolling on click event on the map
  map.addListener('click', enableScrollingWithMouseWheel);

  // Enable scrolling on drag event on the map
  map.addListener('drag', enableScrollingWithMouseWheel);

  // Disable scrolling on mouseout from the map
  map.addListener('mouseout', disableScrollingWithMouseWheel);
}
