// form floating labels

$('.c7-form--field__input').on('input', function() {
  var $field = $(this).closest('.c7-form--field');
  if (this.value) {
    $field.addClass('c7-form--field--not-empty');
  } else {
    $field.removeClass('c7-form--field--not-empty');
  }
});

// Shows or hides contact form

function showForm() {
  document.getElementById("js-form").className += " c7-form--contact__open";
  document.getElementById("body").className += " c7-modal-is-open";
}

function closeForm() {
  document.getElementById("js-form").className =
   document.getElementById("js-form").className.replace
      ( /(?:^|\s)c7-form--contact__open(?!\S)/g , '' )
  document.getElementById("body").className =
   document.getElementById("body").className.replace
      ( /(?:^|\s)c7-modal-is-open(?!\S)/g , '' )
}

// Shows or hides mainmenu

function showMenu() {
  document.getElementById("js-menu").className += " c7-menu--open";
}

function closeMenu() {
  document.getElementById("js-menu").className =
   document.getElementById("js-menu").className.replace
      ( /(?:^|\s)c7-menu--open(?!\S)/g , '' )
}

// Show or hides tabs

function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("c7-tabs__content");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("c7-tabs__link");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" c7-tabs__active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " c7-tabs__active";
}

// Eerste tab op locatiesoverzicht standaard actief maken
document.getElementById("defaultOpen").click();
