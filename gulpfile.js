var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer'); // Default: > 1%, last 2 versions, Firefox ESR
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();

// Build the product HTML
gulp.task('pug', function() {
  return gulp.src('source/product/!(_)*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('build/product'))
    .pipe(browserSync.reload({ stream: true }))
});

// Build the product CSS
gulp.task('sass', function() {
  return gulp.src('source/product/style/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({
    includePaths: [
      'source/core/scss/00-settings',
      'source/core/scss/01-tools',
      'source/core/scss/02-generic',
      'source/core/scss/03-elements',
      'source/core/scss/04-objects',
      'source/core/scss/05-components',
      'source/core/scss/06-themes',
      'source/core/scss/07-utilities',
      'source/product/scss/05-components',
      'source/product/scss/06-themes',
      'source/product/scss'
    ],
    outputStyle: 'compressed'
  }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/product/css'))
    .pipe(browserSync.reload({ stream: true }))
});

// Optimize product images
gulp.task('image', function() {
  return gulp.src('source/product/images/*')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
    ]))
    .pipe(gulp.dest('build/product/images'))
    .pipe(browserSync.reload({ stream: true }))
});

// Minify product JS
gulp.task('js', function() {
  return gulp.src('source/product/scripts/*.js')
    //.pipe(uglify())
    .pipe(gulp.dest('build/product/scripts'))
    .pipe(browserSync.reload({ stream: true }))
});

// Spinup a webserver and refresh the browser
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'build',
      directory: true,
      notify: false
    },
  })
});

// watch pug and sass files for changes and if so compile the files to html and css and reload the browser
gulp.task('watch', function() {
  gulp.watch('source/product/scss/**/*.scss', ['sass']);
  gulp.watch('source/product/**/*.pug', ['pug']);
  gulp.watch('source/product/images/*', ['image']);
  gulp.watch('source/product/scripts/*', ['js']);
  gulp.watch('source/core/components/*.pug', ['pug']);
  gulp.watch('source/core/scss/**/*.scss', ['sass']);
});

// Start the whole shabang
gulp.task('default', ['watch', 'pug', 'image', 'sass', 'js', 'browserSync']);

function swallowError (error) {
  console.log(error.toString())
  this.emit('end')
}
